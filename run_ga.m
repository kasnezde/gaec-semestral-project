function best = run_ga(x, y, NIND, MAXGEN, NVAR, ELITIST, STOP_PERCENTAGE, PR_CROSS, PR_MUT, CROSSOVER, LOCALLOOP, parentfun, REPR, ah1, ah2, ah3, VISFLAG)
% usage: run_ga(x, y, 
%               NIND, MAXGEN, NVAR, 
%               ELITIST, STOP_PERCENTAGE, 
%               PR_CROSS, PR_MUT, CROSSOVER, 
%               ah1, ah2, ah3)
%
%
% x, y: coordinates of the cities
% NIND: number of individuals
% MAXGEN: maximal number of generations
% ELITIST: percentage of elite population
% STOP_PERCENTAGE: percentage of equal fitness (stop criterium)
% PR_CROSS: probability for crossover
% PR_MUT: probability for mutation
% CROSSOVER: the crossover operator
% calculate distance matrix between each pair of cities
% ah1, ah2, ah3: axes handles to visualise tsp
% VISFLAG: false if visualization should not be performed, useful for batch
% testing
% {NIND MAXGEN NVAR ELITIST STOP_PERCENTAGE PR_CROSS PR_MUT CROSSOVER LOCALLOOP REPR}


        GGAP = 1 - ELITIST;
        mean_fits=zeros(1,MAXGEN+1);
        worst=zeros(1,MAXGEN+1);
        Dist=zeros(NVAR,NVAR);
        for i=1:size(x,1)
            for j=1:size(y,1)
                Dist(i,j)=sqrt((x(i)-x(j))^2+(y(i)-y(j))^2);
            end
        end
        % initialize population
        Chrom=zeros(NIND,NVAR);
        for row=1:NIND
            if REPR==1
                Chrom(row,:)=path2adj(randperm(NVAR));
            elseif REPR==3
                Chrom(row,:)=path2ord(randperm(NVAR));
            else
                Chrom(row,:)=randperm(NVAR);
            end
        end
        gen=0;
        % number of individuals of equal fitness needed to stop
        stopN=ceil(STOP_PERCENTAGE*NIND);
        % evaluate initial population
        ObjV = tspfun(Chrom,Dist,REPR);

        best=zeros(1,MAXGEN);
        % generational loop
        while gen<MAXGEN
            sObjV=sort(ObjV);
          	best(gen+1)=min(ObjV);
        	minimum=best(gen+1);
            mean_fits(gen+1)=mean(ObjV);
            worst(gen+1)=max(ObjV);
            for t=1:size(ObjV,1)
                if (ObjV(t)==minimum)
                    break;
                end
            end
            
            vispath = Chrom(t,:);
            switch REPR
                case 1
                    vispath = adj2path(vispath);
                case 3
                    vispath = ord2path(vispath);                
            end
            
            if VISFLAG == true
                visualizeTSP(x,y, vispath, minimum, ah1, gen, best, mean_fits, worst, ah2, ObjV, NIND, ah3);
            end
            
            if (sObjV(stopN)-sObjV(1) <= 1e-15)
                  break;
            end          
        	%assign fitness values to entire population
        	FitnV=ranking(ObjV);
        	%select individuals for breeding
        	SelCh=select(parentfun, Chrom, FitnV, GGAP);
        	%recombine individuals (crossover)
            SelCh = recombin(CROSSOVER,SelCh,PR_CROSS);
                       
            switch REPR
                case 1
                    SelCh=mutateTSP('inversion',SelCh,PR_MUT,REPR);
                case 2
                    SelCh=mutateTSP('displacement',SelCh,PR_MUT,REPR);
                case 3
                    SelCh=mutateTSP('mut_ord_rand',SelCh,PR_MUT,REPR);                           
            end
            %evaluate offspring, call objective function

        	ObjVSel = tspfun(SelCh,Dist,REPR);
            %reinsert offspring into population
        	[Chrom ObjV]=reins(Chrom,SelCh,1,1,ObjV,ObjVSel);

            Chrom = tsp_ImprovePopulation(NIND, NVAR, Chrom,LOCALLOOP,Dist, REPR);
        	%increment generation counter
        	gen=gen+1;            
        end

end
