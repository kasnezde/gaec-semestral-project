%%%%%%%%%%%%%%%%
% TO BE SET
%%%%%%%%%%%%%%%%
NIND=50;		% Number of individuals
MAXGEN=100;		% Maximum no. of generations
PRECI=1;		% Precision of variables
ELITIST=0.05;    % percentage of the elite population
GGAP=1-ELITIST;		% Generation gap
STOP_PERCENTAGE=.95;    % percentage of equal fitness individuals for stopping
LOCALLOOP=0;      % local loop removal
REPR = 2;           % representation (1 - adj, 2 - path, 3 - ord)
PR_CROSS = 0.99;
PR_MUT = 0.5;  

% grid search
dataset_nrs = [ 1 3 5 7 9 ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% no visualization
ah1={};
ah2={};
ah3={};
VISFLAG=false;

% load datasets
datasetslist = dir('datasets/');
datasets=cell( size(datasetslist,1)-2 ,1);
for i=1:size(datasets,1)
    datasets{i} = datasetslist(i+2).name;
end




reprstr = {'adj', 'path', 'ord'};
crossoversstr = {'xalt_edges', 'path_xover', 'nptxover'};
% choose the right crossover
CROSSOVER = char(crossoversstr(REPR));

disp(['REPR = ', char(reprstr(REPR))]);

n = numel(dataset_nrs);
res = zeros(n,2);

for i = 1:n
    DATASET_NR=dataset_nrs(i);
    data = load(['datasets/' datasets{DATASET_NR}]);
    x=data(:,1)/max([data(:,1);data(:,2)]);
    y=data(:,2)/max([data(:,1);data(:,2)]);
    NVAR=size(data,1);
    
    tic;
    best = run_ga(x, y, NIND, MAXGEN, NVAR, ELITIST, STOP_PERCENTAGE, PR_CROSS, PR_MUT, CROSSOVER, LOCALLOOP, 'sus', REPR, ah1, ah2, ah3, VISFLAG);
    time = toc;
    
    bestreal = nonzeros(best);
    res(i,1) = bestreal(end);
    res(i,2) = time;

    disp(['dataset = ', datasets{DATASET_NR}, ' RES = ', num2str(res(i,1)), ' time = ', num2str(res(i,2))]); 
    
end

res
