% low level function for TSP mutation
% specific mutation for ordinal representation: a single integer in the
% path representation is changed
%
% Representation should be always ordinal

function NewChrom = mut_ord_rand(OldChrom,~)

NewChrom=OldChrom;
NVAR=numel(NewChrom);

% picks a position in chromosome in the interval [1 NVAR-1]
% last position is always 1, cannot be mutated
pos = rand_int(1,1,[1 NVAR-1]);
oldint = OldChrom(pos);

% generates new integer from the interval [1,NVAR-pos+1] \ {oldint}
% this ensures that the chromosome is always changed and the ordinal
% representation remains valid
newint = rand_int(1,1,[1,NVAR-pos]);
if newint>=oldint
    newint = newint+1;
end

NewChrom(pos) = newint;

end
