% This function performs tournament selection for GA.
%
% Syntax:  NewChrIx = tournament(FitnV, Nsel)
%
% Input parameters:
%    FitnV     - Column vector containing the fitness values of the
%                individuals in the population.
%    Nsel      - number of individuals to be selected
%
% Output parameters:
%    NewChrIx  - column vector containing the indexes of the selected
%                individuals relative to the original population, shuffled.
%                The new population, ready for mating, can be obtained
%                by calculating OldChrom(NewChrIx,:).

function NewChrIx = tournament(FitnV,Nsel)
    TOURNAMENT_SIZE = 5;    
    Nind = numel(FitnV);
    NewChrIx = zeros(1, Nsel);
    
    for i = 1:Nsel
        idx = randsample(Nind, TOURNAMENT_SIZE);        
        fitvals = FitnV(idx);
        [~, best] = max(fitvals);
        NewChrIx(i) = idx(best);
    end

    % Shuffle new population
   [~, shuf] = sort(rand(Nsel, 1));
   NewChrIx = NewChrIx(shuf); 
end