function neighbours = getneighbours(a,i)
    NVAR = numel(a);
    index = find(a==i);
    
    if index == 1
        neighbours = [a(NVAR) a(2)];
    elseif index == NVAR
        neighbours = [a(NVAR-1) a(1)];
    else
        neighbours = [a(index+1) a(index-1)];
    end
end