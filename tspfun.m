%
% ObjVal = tspfun(Phen, Dist)
% Implementation of the TSP fitness function
%	Phen contains the phenocode of the matrix coded representation
%	Dist is the matrix with precalculated distances between each pair of cities
%	ObjVal is a vector with the fitness values for each candidate tour (=each row of Phen)
%   REPR is the phenocode representation - 1=adj, 2=path, 3=ord

function ObjVal = tspfun(Phen, Dist, REPR)
    % convert represenations to adjacency

    if REPR == 3
        for i=1:size(Phen,1)
            Phen(i,:) = path2adj(ord2path(Phen(i,:)));
        end
    end
    if REPR == 2
        for i=1:size(Phen,1)
            Phen(i,:) = path2adj(Phen(i,:));
        end
    end

	ObjVal=Dist(Phen(:,1),1);
    for t=2:size(Phen,2)
    	ObjVal=ObjVal+Dist(Phen(:,t),t);
    end
end

