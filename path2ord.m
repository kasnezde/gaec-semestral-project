%
% path2ord(Path)
% function to convert between path and ordinal representation for TSP
% path and ord are row vectors
%

function ord = path2ord(path)
    NVAR=numel(path);
	ord=zeros(1,NVAR);
    lst=1:NVAR;
    
    for i=1:NVAR
        idx=find(lst==path(i));
		ord(i) = idx(1);
        lst(ord(i))=[];
    end
end