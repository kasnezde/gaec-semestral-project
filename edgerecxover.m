function newchrom = edgerecxover(a,b)
     NVAR = numel(a);
    newchrom = zeros(1,NVAR);    
    edgemap = getedgemap(a,b);
    current = b(1);    
    
    for t = 1:NVAR
       % add current city to the new chromosome       
       newchrom(t) = current;
       
       % remove all occurences of the current city
       edgemap(edgemap==current) = 0;     
       
       neighbours = nonzeros(edgemap(current,:));
       neigh_cnt = numel(neighbours);

       if neigh_cnt > 0           
           % count remaining cities of the neighbours
           counts = zeros(1, neigh_cnt);       
           for i = 1:neigh_cnt
              counts(i) = nnz(unique(edgemap(neighbours(i),:))); 
           end
           % pick the neighbour with the shortest list
           [~,idx] = min(counts);
           current = neighbours(idx);
       elseif t<NVAR
           % pick a random unvisited city
           remaining = setdiff(1:NVAR,nonzeros(newchrom));
           current = remaining(randi(numel(remaining)));
       end
    end    
end