% low level mutation function for path representation of TSP 
% displacement mutation: a random subtour is removed and inserted at
% another place

function NewChrom = displacement(OldChrom,~)

NewChrom=OldChrom;

NVAR = numel(NewChrom);

cutpos = randi(NVAR-1);
length = randi(NVAR-cutpos);

cut = NewChrom(cutpos:cutpos+length);
NewChrom(cutpos:cutpos+length) = [];

insertpos = rand_int(1,1,[0 numel(NewChrom)]);
NewChrom = [NewChrom(1:insertpos) cut NewChrom(insertpos+1:end)];

end
