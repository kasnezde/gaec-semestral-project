%%%%%%%%%%%%%%%%
% TO BE SET
%%%%%%%%%%%%%%%%
NIND=50;		% Number of individuals
MAXGEN=100;		% Maximum no. of generations
PRECI=1;		% Precision of variables
ELITIST=0.05;    % percentage of the elite population
GGAP=1-ELITIST;		% Generation gap
STOP_PERCENTAGE=.95;    % percentage of equal fitness individuals for stopping
LOCALLOOP=0;      % local loop removal
REPR = 2;           % representation (1 - adj, 2 - path, 3 - ord)
DATASET_NR=7;       % dataset number         
% grid search
pr_cross_list = [0 0.1 0.5 0.9 0.95 0.99];  % crossover probabilities
pr_mut_list = [0 0.05 0.1 0.5 0.9 0.99];        % mutation probabilities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% no visualization
ah1={};
ah2={};
ah3={};
VISFLAG=false;

% load datasets
datasetslist = dir('datasets/');
datasets=cell( size(datasetslist,1)-2 ,1);
for i=1:size(datasets,1)
    datasets{i} = datasetslist(i+2).name;
end

data = load(['datasets/' datasets{DATASET_NR}]);
x=data(:,1)/max([data(:,1);data(:,2)]);
y=data(:,2)/max([data(:,1);data(:,2)]);
NVAR=size(data,1);

n1 = numel(pr_cross_list);
n2 = numel(pr_mut_list);

res = zeros(n1, n2);

reprstr = {'adj', 'path', 'ord'};
crossoversstr = {'xalt_edges', 'path_xover', 'nptxover'};
% choose the right crossover
CROSSOVER = char(crossoversstr(REPR));

disp(['REPR = ', char(reprstr(REPR))]);

for i = 1:n1
    for j = 1:n2
        PR_CROSS = pr_cross_list(i);
        PR_MUT = pr_mut_list(j);
       
        best = run_ga(x, y, NIND, MAXGEN, NVAR, ELITIST, STOP_PERCENTAGE, PR_CROSS, PR_MUT, CROSSOVER, LOCALLOOP, 'sus', REPR, ah1, ah2, ah3, VISFLAG);
        bestreal = nonzeros(best);
        res(i,j) = bestreal(end);
        
        disp(['PR_CROSS = ',num2str(PR_CROSS),' PR_MUT = ', num2str(PR_MUT), ' RES = ', num2str(res(i,j))]); 
    end    
end

res
