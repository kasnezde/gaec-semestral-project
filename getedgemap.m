function edgemap = getedgemap(a,b)
    NVAR = numel(a);
    edgemap = zeros(NVAR, 4);
   
    
    for i = 1:NVAR
        edgemap(i,1:2) = getneighbours(a, i);
        edgemap(i,3:4) = getneighbours(b, i);       
    end
end