% n-point crossover for TSP
% Syntax:  NewChrom = nptxover(OldChrom, XOVR)
%
% Input parameters:
%    OldChrom  - Matrix containing the chromosomes of the old
%                population. Each line corresponds to one individual
%                (in any form, not necessarily real values).
%    XOVR      - Probability of recombination occurring between pairs
%                of individuals.
%
% Output parameter:
%    NewChrom  - Matrix containing the chromosomes of the population
%                after mating, ready to be mutated and/or evaluated,
%                in the same format as OldChrom.
%

function NewChrom = nptxover(OldChrom, XOVR)

if nargin < 2, XOVR = NaN; end

% number of crossover points
NPTS = 1;
   
[rows,cols]=size(OldChrom);
NewChrom = zeros(rows, cols);
   
   maxrows=rows;
   if rem(rows,2)~=0
	   maxrows=maxrows-1;
   end
   
   for row=1:2:maxrows
     % crossover of the two chromosomes
   	% results in 2 offsprings
	if rand<XOVR			% recombine with a given probability                
        pts = rand_int(1,NPTS,[1 cols]);      % generate crossover points
        ptpos = 1;                            % next crossover point
        dir = true;                           % which parent to copy
        
        for i=1:cols
            if ptpos <= NPTS && i == pts(ptpos)
                ptpos = ptpos+1;
                dir = ~dir;  % swap parents
            end
            if dir == true                         % 1st => 1st, 2nd => 2nd
                NewChrom(row,i) = OldChrom(row,i);
                NewChrom(row+1,i) = OldChrom(row+1,i);
            else                                   % 1st => 2nd, 2nd => 1st
                NewChrom(row,i) = OldChrom(row+1,i);
                NewChrom(row+1,i) = OldChrom(row,i);
            end         
        end     
	else
		NewChrom(row,:)=OldChrom(row,:);
		NewChrom(row+1,:)=OldChrom(row+1,:);
	end
   end

   if rem(rows,2)~=0
	   NewChrom(rows,:)=OldChrom(rows,:);
   end

end
