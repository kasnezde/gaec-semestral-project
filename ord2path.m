%
% ord2path(odj)
% function to convert between ordinal and path representation for TSP
% ord, path are row vectors
%

function path = ord2path(ord)
    NVAR=numel(ord);
	path=zeros(1,NVAR);
    lst=1:NVAR;
    
    for i=1:NVAR
        path(i)=lst(ord(i));
        lst(ord(i)) = [];
    end
end


